# README #

Este README foi desenvolvido para treinar comandos no git nas aulas da Proway

### Descrição do que foi pedido ###

Crie um repositório do Bitbucket para seu projeto do curso Desvendando Git e Bitbucket e adicione, confirme e envie uma alteração.

Crie um novo repositório no Bitbucket com nome que desejar e marque para criar o arquivo README.md
Faça um clone deste repositório em sua máquina

Abra o arquivo README.md com o editor de código de sua preferência e faça algumas alterações no texto
Em seu terminal, rode os comandos:

git add para adicionar o arquivo
git commit para commitar o novo arquivo, sem esquecer de adicionar uma mensagem útil e informativa sobre o que será publicado
git push para enviar para o repositório no Bitbucket
